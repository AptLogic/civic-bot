/**Bitlogix CivicBot:tm:
 * Copyright (C) 2019 Bitlogix Technologies
 * Coded in node.js, to run:
 * step 0: insert a Marketcheck and Discord API token in lines 72 and 74
 * npm -i and npm start
 * @type {module:discord.js}
 */
const Discord = require('discord.js');
var client = new Discord.Client();
const request = require('request');
//const apicaller = require('./requestor.js');
//*******DISCORD BOT INITIALIZER********
//Don't touch this unless you want something to break -1001
client.on("ready", ()=>{
    console.log("Connected as " + client.user.tag)
})

client.on('message', (receivedMessage) => {
    if (receivedMessage.author == client.user) { // Prevent bot from responding to its own messages
    return
}

if (receivedMessage.content.startsWith("!")) {
    processCommand(receivedMessage)
}
})

function processCommand(receivedMessage) { //Command Processor (obviously)
    var fullCommand = receivedMessage.content.substr(1); // Remove the leading exclamation mark
    var splitCommand = fullCommand.split(" "); // Split the message up in to pieces for each space
    var primaryCommand = splitCommand[0]; // The first word directly after the exclamation is the command
    var arguments = splitCommand.slice(1); // All other words are arguments/parameters/options for the command

    console.log("Command received: " + primaryCommand);
    console.log("Arguments: " + arguments); // There may not be any arguments

    if (primaryCommand === "help") {
        helpCommand(arguments, receivedMessage)
    } else if (primaryCommand === "civic") {
        getCivic(arguments, receivedMessage)
    } else {
        receivedMessage.channel.send("I don't understand the command. Try `!help`")
    }
}
//***************************************
//*********DISCORD HANDLERS**************
function helpCommand(arguments, receivedMessage) {
    receivedMessage.channel.send("`!civic [zip code]` shows a random 2000-2005 Honda Civic in your ZIP code.")
}

function getCivic(arguments, receivedMessage){
    if(arguments.toString().length<1){
        receivedMessage.channel.send("Please specify a zip code to search in!");
        console.log("Argument empty.");
    } else if(arguments.toString().length===5){
        receivedMessage.channel.send("zip code: " + arguments);
        console.log("Valid zip entered: " + arguments);
        var civicInit = initCivic(arguments); //confusion 100
        civicInit.then(function(result) {
            receivedMessage.channel.send(result);
        }, function(err) {
            console.log(err);
        })
    } else {
        receivedMessage.channel.send("Please specify a valid US zip code!");
        console.log("Argument too long to be a zip code.");
        console.log("Argument length: " + arguments.toString().length);
    }
}
//**************************************
//Discord/Marketcheck API Keys, URL and Search delimiter data
var MKT_API_KEY = "API_KEY_HERE";
var YMMT = "2000%7CHonda%7CCivic%2C2001%7CHonda%7CCivic%2C2002%7CHonda%7CCivic%2C2003%7CHonda%7CCivic%2C2004%7CHonda%7CCivic%2C2005%7CHonda%7CCivic";
var DISCORD_API_KEY = "API_KEY_HERE";

//init promise constructor that will be called by getCivic in order to (re)set the var civicData to up-to-date data per request
//this won't multithread well but we just need the damn thing to work -1001
function initCivic(zipInput){
    var url = 'https://marketcheck-prod.apigee.net/v1/search?api_key='+ MKT_API_KEY + '&zip=' + zipInput + '&radius=20&car_type=used&ymmt=' + YMMT + '&start=0&rows=10&sort_order=asc';
    var options = {
        'url': url,
        'method': 'GET',
        'headers': {
            'Host': 'marketcheck-test.apigee.net'
        }
    };
    return new Promise(function (resolve, reject) {
        //run in async
        request.get(options, function (err, resp, body) {//compared to v0.1 this version returns the body separately from the request as a whole
            if (err) {
                reject(err);
            } else {
                if(JSON.parse(body).num_found===0){resolve("No Civics found within 20 miles of the specified zipcode :(")}
                else{resolve(embedCivicData(JSON.parse(body).listings[Math.floor(Math.random() * JSON.parse(body).listings.length) + 0 ]));} //gotta parse the data before filtering, dumbass
            } // returns pre-parsed data
        })

    })
}

async function embedCivicData(data){
    var embed = new Discord.RichEmbed()
        .setColor('#0099ff')
        .setTitle(data.heading)
        .setURL(data.vdp_url)
        .setAuthor('CivicBot™', 'https://i.imgur.com/wqTJnz4.png', 'https://bitlgx.com')
        .setThumbnail(data.media.photo_links[1])
        .addField('Price', "$" + data.ref_price, true)
        .addBlankField()
        .addField('Mileage', data.miles + " Miles",true)
        .addField('Colour', data.exterior_color, true)
        .addField('Listing Type', data.inventory_type, true)
        .addField('Engine Type', data.build.engine, true)
        .setTimestamp()
        .setFooter('Bitlogix CivicBot™ v0.75', 'https://i.imgur.com/wqTJnz4.png');
    return embed;
}

//sequence to return the callback: set var zip to the input args, call the promise and process into the var civicData with a callback
//after that you should have usable data to work with. -1001
//after the callback var is initialized you _should_ be able to port out the parsing job to a helper file or lib.

client.login(DISCORD_API_KEY);